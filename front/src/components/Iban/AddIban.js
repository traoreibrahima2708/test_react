import React, { Component } from 'react';
import './Iban.css';
import axios from 'axios'
class AddIban extends Component {
	constructor(props) {
		super(props)

		this.state = {
			numeroIban: '',
            errorMsg:''
			
		}
	}

	changeHandler = e => {
		this.setState({ [e.target.name]: e.target.value })
	}

	submitHandler = e => {
		e.preventDefault()
		console.log(this.state)
		axios
			.post('http://localhost:3008c/iban/add', this.state)
			.then(response => {
				console.log(response)
			})
			.catch(error => {
				console.log(error)
                this.setState({errorMsg: alert('IBAN Non Valide')})
			})
	}

	render() {
		const { numeroIban} = this.state
		return (
			<div>
				<form onSubmit={this.submitHandler}>
					<div>
						<input
							type="text"
							name="numeroIban"
							value={numeroIban}
							onChange={this.changeHandler} className="input" placeholder="Entrez un IBAN"
							// {...register("numeroIban", {
							// 	required: true
							//   })}
						/>
						  {/* <p>{errors[input.name]?.message}</p> */}
						
						<p><button type="submit" className="btn">Ajouter</button></p>
						
					</div>
				
					
				</form>
			</div>
		)
	}
}


export default AddIban