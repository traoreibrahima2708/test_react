const validator = require('fastest-validator');
const jwt = require('jsonwebtoken')

function ValidateUserFormat(User) {
    
    let Validator = new validator()

    let schema = {
        name: {
            type: "string",
            max: 40
        },
        email: {
            type: "email",
        } ,
        passWord: {
            type: "string",
            max: 50
        },
        role: {
            type: "string",
            max: 20,
            nullable: true
        }
    }

    return Validator.validate(User, schema);
}


function ValidateIbanFormat(iban) {
    
    let Validator = new validator()

    let schema = {
        numeroIban: {
            type: "string",
            max: 27
        }
      
    }

    return Validator.validate(iban, schema);
}

function fetchUserFromRequest(body) {
    if(hasAllParams(body, ['name', 'email','passWord', 'role'] )){
        return {
            name: body.name,
            email: body.email.toLowerCase(),
            passWord: body.passWord,
            role: body.role
        
        }
    }
    else return undefined
}



function fetchIbanFromRequest(body) {
    if(hasAllParams(body, ['numeroIban'] )){
        return {
            numeroIban: body.numeroIban,   
        }
    }
    else return undefined
}



function decodeToken(Token){
    try {
        const decodedToken = jwt.verify(Token, process.env.JWT_KEY);
        return decodedToken;
    } catch (error) {
        return false;
    }
}

function hasAllParams(req, params) {
    for (let i = 0; i < params.length; i++) {
        if( typeof req[params[i]] == 'undefined' )
            return false;        
    }
    return true;
}

module.exports = {
    ValidateUserFormat: ValidateUserFormat,
    ValidateIbanFormat:ValidateIbanFormat,
   
    fetchIbanFromRequest:fetchIbanFromRequest,
    fetchUserFromRequest: fetchUserFromRequest,
    
    hasAllParams: hasAllParams,
    decodeToken: decodeToken
}