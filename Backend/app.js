//Create Dirs
global.__baseDir = __dirname;
require('./Routes/Dirs');

// IMPORTS 
require('dotenv').config();
const express = require('express');
const BodyParsser = require('body-parser');
const Routes = require(__Routes + 'Routes');
const path =require('path')
// INSTANCING THE SERVER
const app = express();

// MIDDLEWARES
app.use(BodyParsser.json());
app.use(express.json());
app.use(express.static('../front/build'));
app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "*");
        if (req.method === 'OPTIONS') {
                res.header('Access-Control-Allow-Methods', 'PUT, POST,PATCH,DELETE,GET');
                return res.status(200).json({});
        }
        next();
});


//ROUTES
app.get('/iban/xx/*',(_,res)=>{
        res.sendFile(path.join(__dirname, '../front/build/index.html'))
})
app.get('/', (req, res)=> {
        res.sendFile(__views + 'index.html')
 });
app.use('/users', Routes.Users);
app.use('/iban', Routes.Iban);
app.use('/Auth', Routes.Auth);
const PORT= process.env.LISTEN_PORT || 3004
// START LISTENING
app.listen(PORT, () => {
        console.log(`Server is running on port :${PORT}.`);
      });
    