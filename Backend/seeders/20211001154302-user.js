'use strict';

const faker = require('faker');

const usersList =  [];

for (let i = 0; i < 10; i++) {
    usersList.push({
      name: faker.name.firstName(),
     
      email: faker.internet.email(),
    
      passWord: faker.internet.password(),
      role: "USER",
      createdAt : faker.date.past(2) ,
      updatedAt : faker.date.past(2) ,
    })
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
      return queryInterface.bulkInsert('Users', usersList, {});

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    return queryInterface.bulkDelete('Users', null, {})

  }
};
